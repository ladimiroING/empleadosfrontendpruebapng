import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations'; 
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/components/header/header.component';
import { HeaderModule } from './shared/components/header/header.module';
import { PrimeNgModule } from './prime-ng/prime-ng.module';
import { ListaModule } from './pages/empleados/lista/lista.module';
import { NuevoModule } from './pages/empleados/nuevo/nuevo.module';
import { EditarModule } from './pages/empleados/editar/editar.module';
import { MessageService } from 'primeng/api';
import { HomeComponent } from './shared/components/home/home.component';




@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HeaderModule,
    PrimeNgModule,
    ListaModule,
    NuevoModule,
    EditarModule,
    BrowserAnimationsModule
        
  ],
  providers: [MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
