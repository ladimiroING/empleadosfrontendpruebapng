import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetalleComponent } from './pages/empleados/detalle/detalle.component';
import { EditarComponent } from './pages/empleados/editar/editar.component';
import { ListaComponent } from './pages/empleados/lista/lista.component';
import { NuevoComponent } from './pages/empleados/nuevo/nuevo.component';
import { HomeComponent } from './shared/components/home/home.component';

const routes: Routes = [
  {path:'home', component:HomeComponent}, 
  { path: 'lista', component:ListaComponent}, 
  { path: 'nuevo', component: NuevoComponent}, 
  { path: 'editar/:id', component:EditarComponent},
  { path:'detalle/:id', component:DetalleComponent},
  { path:'**',  redirectTo:'home',pathMatch:'full'}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
