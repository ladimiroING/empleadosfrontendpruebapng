import { Component, OnInit } from '@angular/core';



@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  MenuItems=[
    {
      label:'Home', icon:'pi pi-fw pi-home', routerLink: ['/home']
    },
    {
      label:'Agregar Empleado', icon:'pi pi-fw pi-user', routerLink: ['/nuevo']
    },
    {
      label:'Lista de Empleados', icon:'pi pi-fw pi-user', routerLink: ['/lista']
    }
    
  ]

  constructor() { }

  ngOnInit(): void {
    
  }

}
