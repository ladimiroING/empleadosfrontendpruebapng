import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Empleado } from 'src/app/model/empleado';
import { EmpleadosService } from '../empleados.service';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.css']
})
export class DetalleComponent implements OnInit {

  id:number
  empleado:Empleado
  constructor(private route:ActivatedRoute,
             private empleadosServicio:EmpleadosService) { }

  ngOnInit(): void {
    this.id=this.route.snapshot.params['id'];
    this.empleado=new Empleado();
    this.empleadosServicio.getEmpleadoId(this.id).subscribe(dato=>{
      this.empleado=dato
    })
  }

  

}
