import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { DetalleComponent } from './detalle.component';
import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';


@NgModule({
  declarations: [
    DetalleComponent
  ],
  imports: [
    CommonModule,
    PrimeNgModule
    
  ]
})
export class DetalleModule { }
