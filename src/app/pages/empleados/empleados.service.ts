import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Empleado } from 'src/app/model/empleado';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmpleadosService {

  //Esta URL obtiene listado de empleados en el Back
  private apiURL = "http://localhost:8080/api/empleados";
  
  constructor(private httpClient: HttpClient) { }

  //Aplicando metodos Https Request GET, POST, PUT AND DELETE   
  
  getListaEmpleados():Observable<Empleado[]>{
    return this.httpClient.get<Empleado[]>(`${this.apiURL}`);
  }

  getEmpleadoId(id:number):Observable<Empleado>{
    return this.httpClient.get<Empleado>(`${this.apiURL}/${id}`);  
  }

  postEmpleados(empleado: Empleado): Observable<Object>{
    return this.httpClient.post(`${this.apiURL}`,empleado);
  }

  putEmpleado(id:number, empleado:Empleado):Observable<Empleado>{
    return this.httpClient.put<Empleado>(`${this.apiURL}/${id}`,empleado);
  }

  deleteEmpleados(id:number):Observable<Object>{
    return this.httpClient.delete(`${this.apiURL}/${id}`)
  }

}
