import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { NuevoComponent } from './nuevo.component';
import { FormsModule } from '@angular/forms';
import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';



@NgModule({
  declarations: [
    NuevoComponent
  ],
  exports:[
    NuevoComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    PrimeNgModule
  ]
})
export class NuevoModule { }
