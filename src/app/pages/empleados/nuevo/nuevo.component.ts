import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Empleado } from 'src/app/model/empleado';
import { EmpleadosService } from '../empleados.service';

@Component({
  selector: 'app-nuevo',
  templateUrl: './nuevo.component.html',
  styleUrls: ['./nuevo.component.css']
})
export class NuevoComponent implements OnInit {

  empleado:Empleado=new Empleado();
  constructor(private empleadoService:EmpleadosService,
              private router:Router) { }

  ngOnInit(): void {
  }

  saveEmpleado(){
    this.empleadoService.postEmpleados(this.empleado).subscribe(data=>{
      console.log(data);
      this.ListaEmpleados();
    },error=>console.log(error));
  }

  ListaEmpleados(){
    this.router.navigate(['lista']);
  }

  onSubmit(){
    this.saveEmpleado();
  }
}
