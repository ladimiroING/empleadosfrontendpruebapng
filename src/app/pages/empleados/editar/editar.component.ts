import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Empleado } from 'src/app/model/empleado';
import { EmpleadosService } from '../empleados.service';


@Component({
  selector: 'app-editar',
  templateUrl: './editar.component.html',
  styleUrls: ['./editar.component.css']
  
})
export class EditarComponent implements OnInit {

  id:number;
  empleado:Empleado = new Empleado();

  constructor(private emmpleadosService:EmpleadosService,
              private router:Router,
              private route:ActivatedRoute,
              ){}

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.emmpleadosService.getEmpleadoId(this.id).subscribe(dato =>{
      this.empleado = dato;
    },error => console.log(error));
  }

  regresarAListaDeEmpleados(){
    
    this.router.navigate(['lista']);
  }

  

  onSubmit(){
   
    alert("Registro Actualizado")
    this.emmpleadosService.putEmpleado(this.id,this.empleado).subscribe(dato=>{
      this.regresarAListaDeEmpleados();
    },error => console.log(error));
  }

  


}
