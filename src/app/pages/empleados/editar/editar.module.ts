import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { EditarComponent } from './editar.component';
import { FormsModule } from '@angular/forms';
import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';


@NgModule({
  declarations: [
    EditarComponent
  ],
  exports:[
    EditarComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    PrimeNgModule
  ]
})
export class EditarModule { }
