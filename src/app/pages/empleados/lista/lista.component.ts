import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Empleado } from 'src/app/model/empleado';
import { EmpleadosService } from '../empleados.service';
import {ConfirmationService, ConfirmEventType, MessageService} from 'primeng/api';




@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css'],
  providers: [ConfirmationService,MessageService]
  
})
export class ListaComponent implements OnInit {

  empleados:Empleado[];

  
  
  constructor(private empleadoServicio: EmpleadosService,
              private router: Router,
              private messageService: MessageService,
              private confirmationService: ConfirmationService
              ) { }

  ngOnInit(): void {
    this.getEmpleados();
  }

  editarRegistro(id:number){
    this.router.navigate(['editar',id]);    
  }

  detalleRegistro(id:number){
    
    this.router.navigate(['detalle',id]);
  }
  
  eliminarRegistro(id:number){
      this.empleadoServicio.deleteEmpleados(id).subscribe(data=>{
      console.log(data);
      this.getEmpleados();
    })
  }
  
  private getEmpleados(){
    this.empleadoServicio.getListaEmpleados().subscribe((data)=>{
      this.empleados = data;
    })
  }


  eliminar(id:number) {
    this.confirmationService.confirm({
        message: 'Deseas eliminar este registro?',
        header: 'ELIMINAR',
        icon: 'pi pi-info-circle',
        accept: () => {
            this.messageService.add({severity:'info', summary:'Operacion existosa', detail:'Registro eliminado'});
            this.eliminarRegistro(id);  
          },
        reject: (type) => {
            switch(type) {
                case ConfirmEventType.REJECT:
                    this.messageService.add({severity:'error', summary:'Operacion rechazada', detail:'Haz rechazado esta operacion'});
                break;
                case ConfirmEventType.CANCEL:
                    this.messageService.add({severity:'warn', summary:'Operacion cancelada', detail:'Haz cancelado esta operacion'});
                break;
            }
        }
    });
    
  }
  
}
