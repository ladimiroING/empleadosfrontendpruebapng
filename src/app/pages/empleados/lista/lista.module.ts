import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { ListaComponent } from './lista.component';
import { PrimeNgModule } from 'src/app/prime-ng/prime-ng.module';
import {HttpClientModule} from '@angular/common/http'



@NgModule({
  declarations: [
    ListaComponent,
  ],
  exports:[
    ListaComponent
  ],
  imports: [
    CommonModule,
    PrimeNgModule,
    HttpClientModule
  ]
})
export class ListaModule { }
